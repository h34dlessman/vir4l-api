// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require("firebase-functions");

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require("firebase-admin");
admin.initializeApp();
const moment = require("moment");

//↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
//↓↓↓↓↓↓↓↓↓↓↓↓↓↓    ViR4Lz

exports.createDBUser = functions.auth.user().onCreate(async user => {
  const snapshot = await admin
    .firestore()
    .collection("user")
    .doc(user.uid)
    .set({
      joined: admin.firestore.FieldValue.serverTimestamp(),
      name: "fuul",
      votes: []
    });
});

exports.flushDBUser = functions.auth.user().onDelete(async user => {
  const snapshot = await admin
    .firestore()
    .collection("user")
    .doc(user.uid)
    .delete();
});

exports.vote = functions.https.onCall(async (data, context) => {
  const { vote, id, uid } = data;
  const user = await admin
    .firestore()
    .collection("user")
    .doc(uid)
    .get();

  //figure out prev vote
  var votes = user.data().votes;
  const index = votes.findIndex(vote => vote.id === id);
  const memVote = index === undefined || index === -1 ? 0 : votes[index].vote;

  if (memVote === 0) {
    //new vote
    vote === 1 ? upvote(id, 1) : downvote(id, 1);

    const voteObj = {
      id,
      vote
    };
    if (index === undefined || index === -1) {
      votes.push(voteObj);
    } else {
      votes[index] = voteObj;
    }
  } else if (memVote === vote) {
    //unvote
    vote === 1 ? upvote(id, -1) : downvote(id, -1);

    votes.splice(index, 1);
  } else {
    //change vote (unvote old and vote new)
    if (vote === 1) {
      downvote(id, -1);
      upvote(id, 1);
    } else {
      upvote(id, -1);
      downvote(id, 1);
    }

    votes[index].vote = vote;
  }

  //relate vote to user
  admin
    .firestore()
    .collection("user")
    .doc(uid)
    .update({
      votes
    });
});

async function vote(id, value, by) {
  console.log("VOTIN BABY");
  const vid = await admin
    .firestore()
    .collection("video")
    .doc(id)
    .get();
  const snapshot = await admin
    .firestore()
    .collection("video")
    .doc(id)
    .update(
      value === "up"
        ? {
            up: vid.data().up + by
          }
        : {
            down: vid.data().down + by
          }
    );
}

function upvote(id, by) {
  vote(id, "up", by);
}

function downvote(id, by) {
  vote(id, "down", by);
}

exports.voteTitle = functions.https.onCall(async (data, context) => {
  const { titleID, unvoteTitleID } = data;
  console.log("titlevote -- vote: " + titleID + "   unvote: " + unvoteTitleID);

  const tit = await admin
    .firestore()
    .collection("title")
    .doc(titleID)
    .get();
  await admin
    .firestore()
    .collection("title")
    .doc(titleID)
    .update({
      votes: tit.data().votes + 1
    });
  if (unvoteTitleID && unvoteTitleID !== undefined && unvoteTitleID !== "") {
    const unvoteTit = await admin
      .firestore()
      .collection("title")
      .doc(unvoteTitleID)
      .get();
    await admin
      .firestore()
      .collection("title")
      .doc(unvoteTitleID)
      .update({
        votes: unvoteTit.data().votes - 1
      });
  }
});

exports.addVid = functions.https.onCall(async (data, context) => {
  // Grab the text parameter.
  const { id, title } = data;

  const vid = await admin
    .firestore()
    .collection("video")
    .doc(id)
    .get();

  if (vid && vid !== undefined && vid.data() && vid.data() !== undefined) {
    let titles = vid.data().titles;

    const promises = [];
    let titleExists = false;
    for (let i in titles) {
      promises.push(titles[i].get());
    }
    const titleDatas = await Promise.all(promises);

    for (let i in titleDatas) {
      if (titleDatas[i].data().title === title) {
        titleExists = titleDatas[i];
        break;
      }
    }

    if (!titleExists) {
      const newTitleRef = await admin
        .firestore()
        .collection("title")
        .add({
          title,
          votes: 1
        });
      titles.push(newTitleRef);
      const snapshot = await admin
        .firestore()
        .collection("video")
        .doc(id)
        .update({
          titles
        });
    } else {
      let votes = titleExists.data().votes + 1;
      const snapshot = await admin
        .firestore()
        .collection("title")
        .doc(titleExists.id)
        .update({
          votes
        });
    }
  } else {
    // Push the new message into the Realtime Database using the Firebase Admin SDK.
    let titleRef = await admin
      .firestore()
      .collection("title")
      .add({
        title,
        votes: 1
      });
    console.log(titleRef);
    const snapshot = await admin
      .firestore()
      .collection("video")
      .doc(id)
      .set({
        created: admin.firestore.FieldValue.serverTimestamp(),
        down: 0,
        up: 0,
        titles: [titleRef],
        vir4l: ""
      });
  }
});

exports.titulate = functions.https.onCall(async (data, context) => {
  const { titleIDs } = data;

  const promises = [];
  for (let i in titleIDs) {
    promises.push(
      admin
        .firestore()
        .collection("title")
        .doc(titleIDs[i])
        .get()
    );
  }
  const titulos = await Promise.all(promises);

  let winner = titulos[0].data();
  for (let i = 1; i < titulos.length; i++) {
    if (winner.votes < titulos[i].data().votes) {
      winner = titulos[i].data();
    }
  }
  return winner && winner !== undefined ? winner.title : "";
});
